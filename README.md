## To contribute
Fork the this repo then clone from forked repo from your github account 

```git clone https://gitlab.com/your_username/guides.git```  

or   
```git clone git@gitlab.com:your_username/guides.git```

### Setting up environment

* install python of version 2.7 or higher like so:
    ```sudo apt install python3.7```.We are installing version
* install pip using `python get-pip.py`

* install mkdocs `pip install mkdocs`
* to install mkdocs - materials change working directory to clone repo:
 `cd guides` and 
`git clone https://github.com/squidfunk/mkdocs-material.git`

* to ensure you are using current mkdocs-material version `cd mkdocs-material && git reset --hard b9a332c4cdd90c07d685f870bd8cef3d1a4dfb10`

* change your working directory back to guides `cd guides`





### To start your server:

  * with working directory as `guides`
  * run serve like so: `mkdocs serve`

Now you can visit [`localhost:8000`](http://localhost:8000) from your browser.

### Navigating the git

Create a remote repo with name upstream, i.e 

```git remote add upstream https://gitlab.com/podiiacademy/guides.git```

 or

`git remote add upstream git@gitlab.com:podiiacademy/guides.git`

Before working on an issue, ensure your local repo is up-to-date, by running:

* Create a branch from develop like so: `git  checkout -b 3-updatecontribution-readme origin/develop`

* Ensure your work is upto date by pulling from upstream like so: ```git pull upstream develop```

When creating a branch, ensure it has an issue number.

For example, issue `3-update-contribution-readme` should be in the branch name:

`3-update-contribution-readme`




Upon making changes push to the `origin` remote 

```git push origin branchname```

Your last commit message should be in the form `Update contribution readme#fixes3`

Then create a pull request if you feel the issue is completed.


