## Setting up 

Ensure you have ruby and rspec installed in your system.


## Problem Statement

Create an application called Todo,user can create a todo which has many tasks and each task has

who created it(created_by), finishing date, a unique number, done and date created

Done can either be true or false indicating whether the task is finished or not.during addition of task `done` should be false

During creation of todo user can decide to add a task but if no task is added todo will be empty i.e no task.
User can only add one task at time to a todo.

User should be able to know number of days remaining for a todo expire by subracting `date_created` from `finishing_date` .

User should be able to update `done` to `true`   once task is done.

User should be able able to delete a particular task

User should be all to list all the task in a todo

User should to see information a particular todo









### Constraints

created_by should be a string  of  length 1 to 20 characters, it should not contains integers

finishing date should be in following  format "01/02/2020" or "22.02.2020" or "01-02-2020". Don't worry about year, month or day which one comes first, input will always be dd/mm/yyyy.

Unique number of task is only unique for a given todo i.e different tasks in different todos can have same unique number, **hint**: you can use length of current length of todo plus one




## Question

Write a well tested cli application.




 