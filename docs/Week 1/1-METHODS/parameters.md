# Parameters

## Introduction

We used the word parameter in our method definition syntax and didn't care to 
explain guess you googled somewhere and thats alright.Trust me and it challanges even
seasoned developer  this word get misused alot, it get confused with arguement all
time.Enough stories lets do it justice and explain it.Parameter in placeholder or
the variable for actual value during method call,Another buzz word method call
actually you have call methods all this time.
We have three types of parameter

- required 

- defualt

- optional
  
[getty orawo explains parameter and arguement](https://medium.com/podiihq/ruby-parameters-c178fdcd1f4e)

Upon reading this articles lets try tweaking our code

### Exercise 
Kindly using `irb`  on the terminal do these:

- define a method with the name `my_name` which take no parameter and return your name. Call the method 

- Define a method `age_difference` that takes in `age_of_mother` as first parameter and `age_of_daughter` as second parameter and find difference and outputs the difference.

- create a  method have_valid_id? which should have default parameter  of value "No"

- define a method total_deposit this method shoud take any number arguements(desposits, that is each deposit a person has made) and find their sum(use inbuilt method of find sum of array element).