# User is able to deposit

The user should be able to deposit.

For deposit to occur the phone number provide must exist i.e belongs to an existing user.

If account(phone number) do not exist the user should be informed.

If certain amount is deposited balance should increase by that amount for instance if balance was 20.0 and 30.0 is deposited, the new balance is 50.0

User should be given feedback on successfuly deposit for example, "You have successfully deposit 30.00, you new balance is 50.00"