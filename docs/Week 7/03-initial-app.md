# Initializing our application


Let us create a new rails app by running the following

```bash 
$ rails new mpesa-app --database=postgresql --skip-test 

```

From the command we are very explicit that we want our database to be postgresql (rails default database is sqlite3) and we are also skipping setting up test since will be using rspec, the default
test is ussually minitest.

Like I have said we be using RSpec for our test, along side we will use cucumber for behavoir driven development

Worry not we will discuss cucumber in a bit for now just know it just like RSpec helping user in testing
but this case from the user's perspective.

```bash
$ cd mpesa-app
```

Let create our databases: local level(our computer) we can both test and development database.
Run this command to create them.

```bash
$ rails db:create
Created database 'mpesa_app_development'
Created database 'mpesa_app_test'
```
Let run the server at the development environment like so:

```bash
$ rails server
=> Booting Puma
=> Rails 6.0.2.1 application starting in development 
=> Run `rails server --help` for more startup options
Puma starting in single mode...
* Version 4.3.1 (ruby 2.6.3-p62), codename: Mysterious Traveller
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://127.0.0.1:3000
* Listening on tcp://[::1]:3000
Use Ctrl-C to stop
```
When visit <a href="http://localhost:3000" target="_blank">localhost:3000</a> you should see





![you are on rails](./img/hellorails.png)

**Fig 7.0**

It work works properly therefore let us add our changes to git and push them like so:


```bash
$ git add .

$ git commit -m "initial commit"

$ git push origin master
```

