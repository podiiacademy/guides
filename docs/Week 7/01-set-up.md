# Setting Up
For this week we need setup our working environment by:

- installing ruby

- rails gems 

I believe use ruby installed in your computer but just to be use you can always check:

```bash
$ ruby -v
ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-linux]
```
Your result may vary depending on ruby version but you have atleast to 2.6.3

## Installing rails

For unix and linux users it is as straight forward as running

```bash
$ gem install rails
```
confirm successful installation by checking rails version:

```bash 
$ rails -v
Rails 6.0.2.1
```
Our version may vary but version `5.2` and above is ok.



