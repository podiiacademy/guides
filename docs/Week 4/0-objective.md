# Week FOUR

## Objectives
- Understand http protocols
- be to scaffold a simple sinatra based application
- understand importance of test and how to write them
- have basic understanding of relation database sqlite
- master the concept of web hosting

## Day 1  Web communication

### Objectives
- understand request response cycle
- understand http methods
- understand http status code
- understand http encoding
- understand http security 
- understand http caching
We spent some few ago discussing static, client-side web development.This content will be a guide to different form of web develop dynamic web development.This will involves client-server communication.We will use sinatra.The client needs to send request and receive feedback for will take a look at how the data flow from the moment request until feedback is received.

## Day 2 MVC pattern and getting started with sinatra

### Objectives 

- understand separation of concerns and architure of MVC app
- understand workflow of sinatra
- Able to create a simple sinatra app
MVC design pattern specifies that an application consist of a data model, presentation information, and control information. The pattern requires that each of these be separated into different objects.We will take a deep diver and see why it one of most preffered patterns especial when it comes into web development.

## Day 3 CRUD Operations

 ### Objectives
- learn the flow of CRUD operations
- learn how to perform CRUD operations using sinatra
 the CRUD operations enable us to manipulate resources as we please. i.e creating, deleting and more
 

## Day 4 relation database
- understand how to design a database using erd
- understand be able to SQL command.
- understand how to setup database in sinatra app

Data modelling and storage for is one the core if not the most important of aspect of programming.We take deep dive into database design.
## Day 5 Web hosting

### Objectives
- understand domain registration
- understand how to 'point' domain to hosting
- understand difference between production and development enviroment


We will dwell majorly  on how to host websites on the cpanel as well as how to set up emails on the cpanel. We will also go through how to select the best web hosting platform according to your needs i.e what to look for when selecting a web hosting platform.