
# RESTFUL APIS



Representational State Transfer (REST) is a software architectural style that defines a set of 
constraints to be used for creating Web services that is the  CRUD (create, retrieve, update, delete) operations. REST guidelines suggest using a specific HTTP method on a specific type of call made to the server.

HTTP defines a set of request methods to indicate the desired action to be performed for a given resource.Although they can also be nouns, these request methods are sometimes referred as HTTP verbs.Each of them implements a different semantic, but some common features are shared by a group of them: e.g. a request method can be safe, idempotent, or cacheable.


These methods are:

- get

- post

- put

- delete

- patch


## GET
The GET method requests a representation of the specified resource. Requests using GETshould only retrieve data.

##POST

The POST method is used to submit an entity to the specified resource, often causing a change in state or side effects on the server.

## PUT

The PUT method replaces all current representations of the target resource with the request payload.
## DELETE

The DELETE method deletes the specified resource

## PATCH

The PATCH method is used to apply partial modifications to a resource.

There is also a clear summary of [restful API](https://restfulapi.net/http-methods/) what we have just learnt