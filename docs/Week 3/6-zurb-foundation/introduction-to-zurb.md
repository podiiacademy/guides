# Introduction to Zurb Foundation

Foundation is a responsive front-end framework. Foundation provides a responsive grid and HTML and CSS UI components, templates, and code snippets, including typography, forms, buttons, navigation and other interface elements, as well as optional functionality provided by JavaScript extensions.n 

In short zurb is equivalent of `bootstrap`, `material-css` and the likes
 
We we will use Zurb foundation to develop static website.


##  installation

[install](https://foundation.zurb.com/sites/docs/installation.html)

## Starter Project

[starter project](https://foundation.zurb.com/sites/docs/starter-projects.html)


## Zurb file structures

[file structure](https://foundation.zurb.com/learn/foundation-6-stack-file-structure.html)

## understanding zurb template front end

[zurb template](https://zendev.com/2017/09/05/front-end-development-kickstarter-zurb-template.html#scss)

