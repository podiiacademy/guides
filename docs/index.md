 
# Prep Course
 
This a guideline to the student of what they are to what path to follow as they get started with their coding journey. 
## Objectives of this guide

- To familiarize the student with an overview of what programming entails 
- Dig into basic skills of different programming tools and preparing beginners giving them much needed basic skills of programming.
- Laying available programming paths to beginners, what is required to master and rewards of each to enable make an informed decision in the early days of their programming life.
 

 
## CLI
 
This course prepares students to understand the command-line interface, command-line inputs.
- The being used to CLI enable students to understand concepts beyond that which GUI can do. For instance, you can perform tasks by remote login, via secure shell
- CLI allows interacting with your computer with minimal abstraction as possible therefore always letting you tell the computer what exactly you want at low levels. Command also allows to easily view logs of operation going on your computer, therefore, having a better understanding of what is happening.

 
[Get started with CLI](https://www.codecademy.com/learn/learn-the-command-line)
 
## GIT
 
 - Git is a version control system, used to manage different versions of the software. Understanding git technology brings with it a lot of advantages.
 - Repository - you can store your codebase in git both locally(in your computer) and remote (cloud) through a service provider such as GitHub, Gitlab among other therefore acting as a back to your code.
 - Collaboration - git allows to work together with others on project irrespective of where they are, this from provide immense learning opportunity.
 - Revision - version control being the duty of git you can always change version using git.
 
[Get started with git](https://www.codecademy.com/learn/learn-git)
 
 
## Front end
 
### HTML
 
- HTML is the language of the web and all browsers understand and can interpret HTML.
- Therefore understanding HTML is the beginning of understanding how to write a website because technically speaking no HTML no website.HTML is what will help display your website content.
 
[Get started with html](https://www.codecademy.com/learn/learn-html)
 
### CSS
 
- Understanding CSS helps in enhancing the presentation of the website you are creating.
- This enables you to style it, therefore, having consistency in your website.CSS also allow it used to design different viewing option for the website i.e how webpage on different devices depending on size.
 
[Get started with css](https://www.codecademy.com/learn/learn-css)
 
### JavaScript
 
- Javascript is the defacto browser language therefore not set whatsoever required.
 - JS allows for designing interactive applications. Therefore you use it to build games.
- Javascript is everywhere in different forms in the web such as reactjs, vuejs e.t.c, in mobile such as react native, in the desktop app such electron js, therefore, learning it opens unlimited doors of opportunities to explore.
- And if that is not enough statistics indicate that top 2 skills employers are looking for in front end developer is JS.
 
[Get started with JS](https://www.codecademy.com/learn/introduction-to-javascript)
 
## Backend
 

### Ruby
 
- Ruby is a beginner forgiving language yet extremely powerful knowing it will act as launching padding to your programming career.
- Besides its flexibility ruby has the largest community therefore support is readily available.
- Ruby rose to fame due to the rise of its web framework Ruby on rails which is a powerful web building to tool used to make some of the famous websites such as Basecamp, Airbnb, Bleacher, Scribd, Groupon, Gumroad, Hulu, Kickstarter, Pitchfork, Sendgrid, Soundcloud, Square, Yammer, Crunchbase, Slideshare, Funny or Die, Zendesk, Github, Shopify. Trust me I tried brevity.
- For you to have the ability to make such awesome websites understanding ruby is required.
- Through ruby on rails, ruby offers a lot of employment opportunities.
 
[Get started with ruby](https://www.codecademy.com/learn/learn-ruby)
 
### Python
 
- Syntax of python is generally welcoming and the user does not have to do much to get feedback.
- Python is used in web development and most of its famed web frameworks are Django and flask.
- Big data interpretation and visualization using libraries such as numpy, panda have elevated demand for python in the data science world.
- Machine learning and A.I is growing rapidly python supports this through the library such as Scikit-learn.
- Python is also among the top sorted language by the employers.
 
[Get started with python](https://www.codecademy.com/learn/learn-python)
 
 
## Frameworks
### Reactjs
 
- React was developed and maintained by facebook making almost as popular as its parent and it one the rapidly growing and on-demand JS frameworks.
- React is extensively used in well-known applications such as facebook, PayPal Netflix e.t.c.
- React is relatively easy to learn in comparison to its cousin such vuejs that a debate is not getting into trying it.
- You can use reactjs to quickly bootstrap a single page application.
 
 
[Get started with react js](https://www.codecademy.com/learn/paths/build-web-apps-with-react)
 
 
 
 

