# Introduction to react

## What is ReactDOM?

ReactDoM is virtual DOM which get update incase of changes and render it to the browser.That is  upon it's update it compare the changes made with it's original content if there is none it does not render but if there are changes it render.This helps browser to faster.


ReactDOM library has render function which used to render content to the browser.This function takes to arguements, What needs to rendered and where it is rendered i.e `ReactDOM.render(what is to be rendered, where is is rendered)`

## What is JSX?

Create  react app `portofolio`.Delete all the content of source directory like so
`rm src*`

Create index.js file inside `src` directory and whose content should be like so:

*index.js*

```js
  
  import React from "react"
  import ReactDOM from "react-dom"

  // ReactDOM.render(what is to be render, where to render it)
  ReactDOM.render(<p>Hello World</p>, document.getElementById("root"))
```

In the first line we import `react` which allow us to use some magic html `<p>Hello World</p>` inside a javascript file.This what is know jsx, for more on jsx look at [docs](https://reactjs.org/docs/introducing-jsx.html)

The content must be rendered inside a browser which displays using html. The html to rendered must have an element whose id is root since where the content is to be render dictates so.

so let us create the html file like so:

*index.html*

```html
<html>
    <head>
        
    </head>
    <body>
        <div id="root"></div>
        <script src="index.js"></script>
    </body>
</htm
```
Assuming the server is running visit `localhost:3000`.

When the elements are  more one that related element have to wrap around  single element  prefferably `div`

*index.js*

```js
  
  import React from "react"
  import ReactDOM from "react-dom"

  // ReactDOM.render(what is to be render, where to render it)
  ReactDOM.render(<div><h1>Hello World</h1> <p>This is multi-element render</p></div>, document.getElementById("root"))
```

Render this 

My shopping List
 
  * maize
   
  * beans
  
  * rice
  
  * soya 

JSX also allow us to inject js file inside these html element for instance we wanted to ensure `Hello World` in our  previous example is all characters are in uppercase.

*index.js*

```js
//..
//..

  ReactDOM.render(<div><h1>{"Hello World".toUpperCase()}</h1> <p>This is multi-element render</p></div>, document.getElementById("root"))
```

Example is trivial but you get the point


## Functional Components

You can render entire application while writing your code inside the ReactDOM.render function.That is when components coming they allow to break code int modules(parts) which are reusable.Functional components are just regular javascript function which whose intent is ensure reusability.[docs](https://reactjs.org/docs/components-and-props.html)


Let us create the list you created in a functional component.As you can see it is just a regular function. 

*index.js*

```js
//..
//..
function shoppinglist()(
  return(
    <div>
    <h1>My shopping List</h1>
    <ul>
      <li>maize</li>
      <li>beans</li>
      <li>rice</li>
      <li>soya</li>
    <ul>
    </div>
  )
) 
ReactDOM.render(<shoppinglist/>, document.getElementById("root"))
```
We then pass our component as the first aurguement of render function.

Just like elements when components are more than one we wrapp them inside div tag when we pass them to ReactDOM.render function since it is expecting only two aurguements.

In that spirit create a react functional component `aboutMe`.Which contains details about you in nutshell.The component should be rendered together with 
`shoppingList` component.

## 

Our index.js is becoming flooded with code as matter fact the only thing which be there is the render function let to it justice and move our component to separate files

inside src directory create files `aboutMe.js` and `shoppingList.js` move aboutMe component to aboutMe.js file and shoppingList component to shoppingList.js file.Don't forget to import react i.e ` import React from "react"` on both file we need it since we are using JSX.

At the bottom we  `ES6` export to make our component available to other file which might want to use it.In this case our index.js wants to render so let us
 expose them. In nutshell your file should look like so:

*aboutMe.js*

```js
// import react
// aboutMe component

export default aboutMe
```
Do the same for shoppingList component.

Since we exported our component we must import it is more like a trade.We import it in the file we want to us it in this case index.js

*index.js*
```js
//import react
//import ReactDOM
import aboutMe from "./aboutMe"
// Render function
```
What exactly is going on here you ask we are importing `aboutMe` component which is located in `aboutMe.js` file which isin the same directory as file it is being imported to hence `./aboutMe.js` .The file extension `.js` is left out because technically import  a module not a file but putting the extension has not harm you should try it out.But desist from it as it is   not practise.


Now create a third component with your clearly layed out the component should be inside `skills.js` file and should be rendered.

As common practise dictates create a directory called `Components` move all your components(files) there.Also create `App.js` file inside src directory inside this create a functional component `App` , this component should render `shoppingList`, `skills` and `aboutMe` components.The edit `index.js` file to render only `App` component.Hint use `import` and `export` statements


